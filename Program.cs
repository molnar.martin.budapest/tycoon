﻿using System;
using System.Windows.Input;
using System.Collections.Generic;

namespace tycoon2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.CursorVisible = false;
            Console.Clear();
            int player1 = 1;
            int player2 = 2;

            //Darab, név, kesfló, bájprájsz, mardzsin
            Factory junk = new Factory(1, "junk food" , 1, 10, 2);
            Factory donut = new Factory(0, "donut" , 5, 100, 5);
            Factory gun = new Factory(0, "gun factory" , 10, 500, 25);
            Factory range = new Factory(0, "gun range" , 20, 1000, 50);
            List<Factory> USfactorys = new List<Factory>()
            {
                junk, donut, gun, range
            };
            Factory biscuits = new Factory(1, "biscuit bakery" , 1, 10, 2);
            Factory tee = new Factory(0, "tee plantage" , 5, 100, 5);
            Factory umbrella = new Factory(0, "umbrella factory", 10, 500, 25);
            Factory colony = new Factory(0, "colony" , 20, 1000, 50);
            List<Factory> UKfactorys = new List<Factory>()
            {
                biscuits, tee, umbrella, colony
            };

            Player p1 = new Player(player1, USfactorys);
            Player p2 = new Player(player2, UKfactorys);

            Timer tick = new Timer(p1, p2);
            //PLAYER FIELD
            
            for(int i = 0; i < 10; i++)
            {
                Console.SetCursorPosition(59, i);
                System.Console.Write("|");
            }
            tick.Print();
            /* 
            Console.SetCursorPosition(0, 2);
            System.Console.WriteLine("b p Junk food");
            System.Console.WriteLine("b p Dounut");
            System.Console.WriteLine("b p Gun range");
            System.Console.WriteLine("b p Gun factory");
            System.Console.Write("b p Building the wall");

            //England
            Console.SetCursorPosition(60, 2);
            System.Console.WriteLine("b p Tea");
            Console.SetCursorPosition(60, 3);
            System.Console.WriteLine("b p Cookies");
            Console.SetCursorPosition(60, 4);
            System.Console.WriteLine("b p Umbrella factory");
            Console.SetCursorPosition(60, 5);
            System.Console.WriteLine("b p Colony");
            Console.SetCursorPosition(60, 6);
            System.Console.Write("b p Brexit");
*/
            for(;;)
            {
                tick.Tick();
            }
        }
    }
    class Timer
    {
        Player p1;
        Player p2;
        int p1_position_x = 0;
        int p1_position_y = 0;
        int p2_position_x = 0;
        int p2_position_y = 0;
        public Timer(Player p1, Player p2)
        {
            this.p1 = p1;
            this.p2 = p2;
        }
        long tick_start = DateTime.Now.Ticks;
        public void Tick()
        { 
            long temporary = DateTime.Now.Ticks-tick_start;

            //Console.SetCursorPosition(p1_position_x, p1_position_y);
            
            //CONTROLL
            if(Console.KeyAvailable)
            {
            ConsoleKeyInfo a = Console.ReadKey(true);
            
            //Cheat
            if(a.KeyChar == 'c')
            {
                p1.gold += 99999;
            }

            //BUY P1
            if(a.KeyChar == '1')
            {
                p1.Buy(0);
            }
            if(a.KeyChar == '2')
            {
                p1.Buy(1);
            }
            if(a.KeyChar == '3')
            {
                p1.Buy(2);
            }
            if(a.KeyChar == '4')
            {
                p1.Buy(3);
            }
            
            //SELL P1
            if(a.KeyChar == 'q')
            {
                p1.Sell(0);
            }
            if(a.KeyChar == 'w')
            {
                p1.Sell(1);
            }
            if(a.KeyChar == 'e')
            {
                p1.Sell(2);
            }
            if(a.KeyChar == 'r')
            {
                p1.Sell(3);
            }
            if(a.KeyChar == 't')
            {
                p1.Sell(4);
            }

            //P2 BUY

            if(a.KeyChar == 'k')
            {
                p2.Buy(0);
            }
            if(a.KeyChar == 'l')
            {
                p2.Buy(1);
            }
            if(a.KeyChar == 'é')
            {
                p2.Buy(2);
            }
            if(a.KeyChar == 'á')
            {
                p2.Buy(3);
            }
            if(a.KeyChar == 'ű')
            {
                p2.Buy(4);
            }
            //SELL P2
            if(a.KeyChar == 'n')
            {
                p2.Sell(0);
            }
            if(a.KeyChar == 'm')
            {
                p2.Sell(1);
            }
            if(a.KeyChar == ',')
            {
                p2.Sell(2);
            }
            if(a.KeyChar == '.')
            {
                p2.Sell(3);
            }
            if(a.KeyChar == '-')
            {
                p2.Sell(4);
            }
            }
            
            Random rnd = new Random();
            //AUTO GOLD
            if(temporary >= 10000000)
            {
                if(p1.gold >= 1000000)
                {
                    System.Console.WriteLine("America builds the Wall!!!");
                    Environment.Exit(0);
                }

                if(p2.gold >= 1000000)
                {
                    System.Console.WriteLine("Brexit time!!!");
                    Environment.Exit(0);
                }
                
                for(int i = 0; i < 4; i++)
                {
                    p1.gold += p1.GetFactory(i).GetCashFlow()*p1.GetFactory(i).GetPiece();
                }

                for(int i = 0; i < 4; i++)
                {
                    p2.gold += p2.GetFactory(i).GetCashFlow()*p2.GetFactory(i).GetPiece();
                }
                if(temporary >= 10000000)
                {
                    int randomNumber = rnd.Next(0, 4);
                    if (p1.rnd_countdown < 10)
                    {
                        p1.rnd_countdown++;
                    }
                    else
                    {
                        p1.rnd_countdown = 0;
                        p1.GetFactory(randomNumber).Random();
                        p2.GetFactory(randomNumber).Random();
                    }  
                    
                }
                //p2.gold += 1;
                tick_start = DateTime.Now.Ticks;
                Print();
            }
            
            return;
        }
        public void Second()
        {

        }
        public void Print()
        {
            Console.SetCursorPosition(0, 0);
            System.Console.Write("Cash: ");
            System.Console.WriteLine(p1.GetGold()+"          ");

            System.Console.WriteLine("Buy  Sell Piece Cash flow Name");
            for(int i = 0; i < 4; i++)
            {
                System.Console.WriteLine(
                    "{0,4} {1,4} {2,5} {3,9} {4}", 
                    p1.GetFactory(i).GetBuyprice(),
                    p1.GetFactory(i).GetSellprice(), 
                    p1.GetFactory(i).GetPiece(),
                    p1.GetFactory(i).GetCashFlow(),
                    p1.GetFactory(i).GetName());
            }
            
            

            Console.SetCursorPosition(60, 1);

            System.Console.WriteLine("Buy  Sell Piece Cash flow Name");
            for(int i = 0; i < 4; i++)
            {
                Console.SetCursorPosition(60, 2+i);
                System.Console.WriteLine(
                    "{0,4} {1,4} {2,5} {3,9} {4}", 
                    p2.GetFactory(i).GetBuyprice(),
                    p2.GetFactory(i).GetSellprice(), 
                    p2.GetFactory(i).GetPiece(),
                    p2.GetFactory(i).GetCashFlow(), 
                    p2.GetFactory(i).GetName());
            }
            Console.SetCursorPosition(60, 6);
            System.Console.Write("Brexit = 1 million Ł");
            

            /*
            System.Console.WriteLine();
            System.Console.WriteLine(p1.GetFactory(0).GetName() + " quantity: " + 
            p1.GetFactory(0).GetPiece() + " buy price: " + p1.GetFactory(0).GetBuyprice() +
            " sell price: " + p1.GetFactory(0).GetSellprice());
            */
            Console.SetCursorPosition(60, 0);
            System.Console.Write("Quid: ");
            System.Console.WriteLine(p2.GetGold() + "       ");

        }
    }
    class Player
    {
        public int rnd_countdown;
        int player;
        List<Factory> factories;
        public int gold = 0;
        public Player(int player, List<Factory> fact)
        {
            this.player = player;
            this.factories = fact;
        }
        public void Buy(int index)
        {
            if(gold >= factories[index].GetBuyprice())
            {
                factories[index].buy();
                gold -= factories[index].GetBuyprice();
            }
        }
        public void BuyWin()
        {
            if(gold >= 1000000)
            {
                System.Console.WriteLine("Player: " + player + "wins.");
                Environment.Exit(0);
            }
        }
        public void Sell(int index)
        {
            if(factories[index].GetPiece() > 0)
            {
                factories[index].sell();
                gold += factories[index].GetSellprice();
            }
        }
        public Factory GetFactory(int index)
        {
            return factories[index];
        }
        
        public int GetGold()
        {
                return gold;
        }
    }
    class Factory
    {
        int piece;
        string name;
        int cashflow;
        int buyprice;
        int margin;
        
        Random random = new Random();
        public Factory(int piece, string name, int cashflow, int buyprice, int margin)
        {
            this.piece = piece;
            this.name = name;
            this.cashflow = cashflow;
            this.buyprice = buyprice;
            this.margin = margin;
        }
        public void buy()
        {
            piece++;
            ChangePrice(buyprice/10);
        }
        public void sell()
        {
            if(piece > 0)
            {
                piece--;
            }
        }
        public void ChangePrice(int delta)
        {
            buyprice += delta;
        }
        public void ChangeCashFlow(int delta)
        {
            cashflow += delta;
        }
        public int GetBuyprice()
        {
            return buyprice;
        }
        public int GetSellprice()
        {
            return buyprice-margin;
        }
        public string GetName()
        {
            return name;
        }
        public int GetCashFlow()
        {
            return cashflow;
        }
        public int GetPiece()
        {
            return piece;
        }
        public void Random()
        {
            int randomNumber = random.Next(0, 2);
            if(randomNumber == 0)
            {
                ChangeCashFlow(1);
            }
            else
            {
                ChangeCashFlow(-1);
            }
            
        }
    }
}